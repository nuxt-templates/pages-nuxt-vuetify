import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.teal.darken2,
    accent: colors.teal.accent2,
    secondary: colors.deepOrange.lighten1
  }
})
